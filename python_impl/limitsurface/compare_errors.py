#!/usr/lib/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2017-2024 Autonomous Robots and Cognitive Systems Laboratory
# Author: Daniel Garcia-Vaglio <degv364@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from numpy import sqrt, pi


from limitsurface.circle_ls import CircleLS
from limitsurface.rectangle_ls import RectangleLS
from limitsurface.ring_ls import RingLS


def calculate_errors(
        friction_coef,
        mass,
        radius,
        side,
        circle_region_length,
        rect_region_length,
        circle_step,
        rect_step):
    circle = CircleLS(radius, friction_coef, mass, circle_region_length,
                      circle_step, eps=0.001)
    rectangle = RectangleLS(side, side, friction_coef, mass,
                            rect_region_length, rect_step, eps=0.001)

    print("*"*80)
    print("Completed a calculation")
    return (circle.approximation_error(), rectangle.approximation_error())


def test_ring():
    friction_coef = 0.5
    mass = 1
    radius = 1
    region_length = 2
    step = 2/10
    percentage = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.999]

    error = []
    for per in percentage:
        ring = RingLS(radius,
                      radius * per,
                      friction_coef,
                      mass,
                      region_length,
                      step,
                      eps=0.001)
        error.append(ring.approximation_error())
    return error


def test_rectangle():
    friction_coef = 0.5
    mass = 1
    side = 1
    region_length = 2
    step = 2/10
    percentage = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]

    error = []
    for per in percentage:
        rect = RectangleLS(side,
                           side * per,
                           friction_coef,
                           mass,
                           region_length,
                           step,
                           eps=0.001)
        error.append(rect.approximation_error())
    return error


def main():
    sample_amounts = [
        8, 10, 12, 14, 16, 18, 20, 30, 40, 50, 60, 70, 80, 90, 100]

    sample_amounts = [8, 10]

    friction_coefs = [0.1, 0.2, 0.3, 0.4, 0.5]
    mass = [0.125, 0.25, 0.5, 1, 2, 4, 8, 16, 32]
    circle_radius = [0.03125, 0.0625, 0.125, 0.25, 0.5, 1, 2, 4, 8, 16, 32]
    rect_sides = [radius * sqrt(pi) for radius in circle_radius]

    circle_region_lengths = [radius * 2 for radius in circle_radius]
    rect_region_lengths = [side * 1.5 for side in rect_sides]

    circle_steps = [length / sample_amounts[1]
                    for length in circle_region_lengths]
    rect_steps = [length / sample_amounts[1] for length in rect_region_lengths]

    # Change friction
    print("Starting Friction tests")
    friction_results = [calculate_errors(
        frict,
        mass[3],
        circle_radius[3],
        rect_sides[3],
        circle_region_lengths[3],
        rect_region_lengths[3],
        circle_steps[3],
        rect_steps[3]) for frict in friction_coefs]

    # Change mass
    print("Starting Mass tests")
    mass_results = [calculate_errors(
        friction_coefs[-1],
        ma,
        circle_radius[3],
        rect_sides[3],
        circle_region_lengths[3],
        rect_region_lengths[3],
        circle_steps[3],
        rect_steps[3]) for ma in mass]

    # Change base
    print("starting Base tests")
    base_results = [calculate_errors(
        friction_coefs[-1],
        mass[3],
        circle_radius[idx],
        rect_sides[idx],
        circle_region_lengths[idx],
        rect_region_lengths[idx],
        circle_steps[idx],
        rect_steps[idx]) for idx in range(len(circle_radius))]

    # Change the amount of points
    print("Starting Amount tests")
    circle_steps = [circle_region_lengths[3] / amount
                    for amount in sample_amounts]
    rect_steps = [rect_region_lengths[3] / amount for amount in sample_amounts]
    amount_results = [calculate_errors(
        friction_coefs[-1],
        mass[3],
        circle_radius[3],
        rect_sides[3],
        circle_region_lengths[3],
        rect_region_lengths[3],
        circle_steps[idx],
        rect_steps[idx]) for idx in range(len(sample_amounts))]

    print("Starting rectangle tests")
    rectangle_results = test_rectangle()

    print("Starting ring tests")
    ring_results = test_ring()

    # print results
    print("Friction")
    for idx in range(len(friction_coefs)):
        print(friction_coefs[idx],
              round(friction_results[idx][0][0], 4),
              round(friction_results[idx][0][1], 4),
              round(friction_results[idx][1][0], 4),
              round(friction_results[idx][1][1], 4))

    print("Mass")
    for idx in range(len(mass)):
        print(mass[idx],
              round(mass_results[idx][0][0], 4),
              round(mass_results[idx][0][1], 4),
              round(mass_results[idx][1][0], 4),
              round(mass_results[idx][1][1], 4))
    print("base")
    for idx in range(len(circle_radius)):
        print(circle_radius[idx],
              round(rect_sides[idx], 4),
              round(base_results[idx][0][0], 4),
              round(base_results[idx][0][1], 4),
              round(base_results[idx][1][0], 4),
              round(base_results[idx][1][1], 4))
    print("Amount")
    for idx in range(len(amount_results)):
        print(sample_amounts[idx]**2,
              round(amount_results[idx][0][0], 4),
              round(amount_results[idx][0][1], 4),
              round(amount_results[idx][1][0], 4),
              round(amount_results[idx][1][1], 4))

    print("Rectangle")
    for result in rectangle_results:
        print(round(result[0], 4),
              round(result[1], 4))

    print("Ring")
    for result in ring_results:
        print(round(result[0], 4),
              round(result[1], 4))
