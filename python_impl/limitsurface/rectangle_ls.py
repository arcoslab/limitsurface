# -*- coding: utf-8 -*-
#
# Copyright (c) 2017-2024 Autonomous Robots and Cognitive Systems Laboratory
# Author: Daniel Garcia-Vaglio <degv364@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from copy import copy
from numpy import sqrt, arcsinh
from limitsurface.limit_surface import LimitSurface
from limitsurface.constants import GRAVITY as G


class RectangleLS(LimitSurface):
    def __init__(self,
                 width,
                 height,
                 friction_coefficient,
                 mass,
                 region_length,
                 step,
                 eps=0.00001):
        """
        Create the limit surface for an object with a rectangular base

        :param width: length of one side
        :param height: length of the other side
        :param friction_coefficient: Dynamic friction coefficient
        :param mass: object's mass
        :param region_length: limits where COR (Center Of Rotation) are taken
        :param step: space between consecutive CORs
        :param eps: (optional) tolerance
        """
        LimitSurface.__init__(self, friction_coefficient, mass, region_length,
                              step, eps=eps)

        self.width = width
        self.height = height
        self._bound_params = [width, height]

    @property
    def mmax(self):
        """
        Return the maximum rotation "force" for the rectangle.

        This is a property because it depends on the base parameters and
        should not be overwritten
        """
        if self._mmax is None:
            p = self.mass * G / (self.width * self.height)
            const = self.friction_coefficient * p / 12
            l1, l2 = [self.height, self.width]
            ret = (l2**3 * arcsinh(l1 / l2) + l1**3 * arcsinh(l2 / l1) +
                   2 * l1 * l2 * sqrt(l2**2 + l1**2))
            self._mmax = const * ret

        return copy(self._mmax)

    @property
    def area(self):
        return self.width * self.height

    @classmethod
    def _bound_functions(cls, width, height):
        return (
            -width/2,  # x_low
            width/2,  # x_high
            lambda x: -height/2,  # y_low
            lambda x: height/2)  # y_high
