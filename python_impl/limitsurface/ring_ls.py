# -*- coding: utf-8 -*-
#
# Copyright (c) 2017-2024 Autonomous Robots and Cognitive Systems Laboratory
# Author: Daniel Garcia-Vaglio <degv364@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from numpy import sqrt, pi
from limitsurface.limit_surface import LimitSurface


class RingLS(LimitSurface):
    def __init__(self,
                 outer_radius,
                 inner_radius,
                 friction_coefficient,
                 mass,
                 region_length,
                 step,
                 eps=0.00001):
        """
        Create the limit surface for an object with a ring base

        :param outer_radius: outer radius
        :param inner_radius: inner radius, the hollow part
        :param friction_coefficient: Dynamic friction coefficient
        :param mass: object's mass
        :param region_length: limits where COR (Center Of Rotation) are taken
        :param step: space between consecutive CORs
        :param eps: (optional) tolerance
        """
        LimitSurface.__init__(self, friction_coefficient, mass, region_length,
                              step, eps=eps)
        self.outer_radius = outer_radius
        self.inner_radius = inner_radius
        self._bound_params = [self.outer_radius]
        self._inner_bound_params = [self.inner_radius]

    @property
    def area(self):
        """
        Get the rings are
        """
        return (self.outer_radius**2 - self.inner_radius**2) * pi

    @classmethod
    def _bound_functions(cls, radius):
        return (
            -radius,  # x_low
            radius,  # x_high
            lambda x: -sqrt(radius**2 - x**2),  # y_low
            lambda x: sqrt(radius**2 - x**2))  # y_high

    @classmethod
    def _inner_bound_functions(cls, radius):
        return (
            -radius,  # x_low
            radius,  # x_high
            lambda x: -sqrt(radius**2 - x**2),  # y_low
            lambda x: sqrt(radius**2 - x**2))  # y_high
