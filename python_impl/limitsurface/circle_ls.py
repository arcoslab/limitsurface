# -*- coding: utf-8 -*-
#
# Copyright (c) 2017-2024 Autonomous Robots and Cognitive Systems Laboratory
# Author: Daniel Garcia-Vaglio <degv364@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from copy import copy
from numpy import sqrt, pi
from limitsurface.limit_surface import LimitSurface
from limitsurface.constants import GRAVITY as G


class CircleLS(LimitSurface):
    def __init__(self,
                 radius,
                 friction_coefficient,
                 mass,
                 region_length,
                 step,
                 eps=0.00001):
        """
        Create the limit surface for an object with a circular base

        :param radius: radius of the base
        :param friction_coefficient: Dynamic friction coefficient
        :param mass: object's mass
        :param region_length: limits where COR (Center Of Rotation) are taken
        :param step: space between consecutive CORs
        :param eps: (optional) tolerance
        """
        LimitSurface.__init__(self, friction_coefficient, mass, region_length,
                              step, eps=eps)
        self.radius = radius
        self._bound_params = [radius]

    @property
    def mmax(self):
        """
        Return the maximum rotation "force" for the circle.

        This is a property because it depends on the base parameters and
        should not be overwritten
         """
        if self._mmax is None:
            const = 2 * self.mass * G * self.friction_coefficient / 3
            self._mmax = self.radius * const

        return copy(self._mmax)

    @property
    def area(self):
        """
        Get the circle's area
        """
        return self.radius**2 * pi

    @classmethod
    def _bound_functions(cls, radius):
        return (
            -radius,  # x_low
            radius,  # x_high
            lambda x: -sqrt(radius**2 - x**2),  # y_low
            lambda x: sqrt(radius**2 - x**2))  # y_high
