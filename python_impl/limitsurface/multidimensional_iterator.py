#!/usr/lib/python3
# -*- coding: utf-8 -*-
#
# Copyright (c) 2017-2018 Autonomous Robots and Cognitive Systems Laboratory
# Author: Daniel Garcia-Vaglio <degv364@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


def _iden(x):
    """
    Identity function, the default function to use with the multidimensional
    iterator
    """
    return x


class MultidimensionalIterator(object):
    """
    Iterates over many dimensions.

    This was built to work with multiprocessing. The idea is to be able to
    iterate over many dimensions, and also have some constants available.
    The current aproach is to pass pool.map a list with a lot of repeated
    arguments to be able to pass constants as arguments. This will
    increase our memory efficiency.
    """

    def __init__(self, ndim_bounds, constants, function=_iden):
        """
        Construct the multidimensional iterator

        :param ndim_bounds: a list of 3-tuples. One for each dimension.
        each tuple should have: (low, high, step). Closed limits.
        :param constants: stores some contatns that are returned with every
        iteration
        :param function: a function to apply to each point (per dimension),
        if a uniform space is not desired
        """
        self.dimensions = len(ndim_bounds)
        # store the current state of each dimension
        self.current_counts = [bound[0] for bound in ndim_bounds]
        # store the lows for each dimension
        self.lows = [bound[0] for bound in ndim_bounds]
        # store the highs for each dimension
        self.highs = [bound[1] for bound in ndim_bounds]
        # store the steps
        self.steps = [bound[2] for bound in ndim_bounds]
        # Store the constants
        self.constants = constants
        # Store the function
        self.function = function

    def __iter__(self):
        # update always happens. This is to compensate first update
        self.current_counts[0] -= self.steps[0]
        return self

    def __next__(self):
        """
        Get the next value in the interation

        iterates over fisrt dimension, once it is completed
        it resetes and goes to the second value of the second dimension
        and so on with N dimensions
        """

        # Update dimension 0, and this will propagate to the rest
        # dimensions if necessary
        self._update_dimension(0)
        # Return a copy of counts not a reference
        return (list(map(self.function, self.current_counts)), self.constants)

    def _update_dimension(self, n):
        """
        update the state of dimension n

        If dimension n reaches the limit, reset it and update the next dim
        :param n: dimension to update
        """
        if n < self.dimensions:
            self.current_counts[n] += self.steps[n]
            if self.current_counts[n] > self.highs[n]:
                self.current_counts[n] = self.lows[n]
                self._update_dimension(n+1)
        else:
            # If we try to update dim+1, means that all the
            # dimensions below have reached high
            raise StopIteration
