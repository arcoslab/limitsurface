# -*- coding: utf-8 -*-
#
# Copyright (c) 2017-2024 Autonomous Robots and Cognitive Systems Laboratory
# Author: Daniel Garcia-Vaglio <degv364@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from numpy import sqrt
from limitsurface.limit_surface import LimitSurface


class HexagonLS(LimitSurface):
    def __init__(self,
                 circumscribed_radius,
                 friction_coefficient,
                 mass,
                 region_length,
                 step,
                 eps=0.00001):
        """
        Create the limit surface for an object with a regular  hexagonal base

        :param circumscribed_radius: radius of a circumference that touches all
                                     vertices
        :param friction_coefficient: Dynamic friction coefficient
        :param mass: object's mass
        :param region_length: limits where COR (Center Of Rotation) are taken
        :param step: space between consecutive CORs
        :param eps: (optional) tolerance
        """

        LimitSurface.__init__(self, friction_coefficient, mass, region_length,
                              step, eps=eps)

        self.circumscribed_radius = circumscribed_radius  # for external use
        self._bound_functions = [self.circumscribed_radius]

    @property
    def area(self):
        """
        Return the are of the hexagon
        """
        return 3 * sqrt(3) * self.circumscribed_radius**2 / 2

    @classmethod
    def _bound_functions(cls, radius):
        def bound(x, radius):
            if x < -radius / 2:
                return sqrt(3) * x + sqrt(3) * radius
            if -radius / 2 <= x <= radius / 2:
                return radius * sqrt(3) / 2
            if x > radius / 2:
                return -sqrt(3) * x + sqrt(3) * radius

        return (
            -radius, radius,
            lambda x: -bound(x, radius),
            lambda x: bound(x, radius))
