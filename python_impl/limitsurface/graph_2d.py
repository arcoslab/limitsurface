#!/usr/lib/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2017-2024 Autonomous Robots and Cognitive Systems Laboratory
# Author: Daniel Garcia-Vaglio <degv364@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import matplotlib.pyplot as plt
from math import tanh, atan, pi, sqrt


from limitsurface.circle_ls import CircleLS


def norm(x, y):
    return sqrt(x**2+y**2)


def one_over_x_truncated(x):
    if abs(x) < 1:
        return x
    else:
        return 1/abs(x)


def torque_approx(x, mmax, r):
    if abs(x) < 1:
        return mmax*(1-2/pi*atan(1.3*(x/r)**2))
    else:
        return mmax/(2.5*abs(x/r))


def main():
    friction_coef = 0.5
    mass = 1
    radius = 2
    region_length = radius * 10
    step = region_length/100

    circle = CircleLS(radius,
                      friction_coef,
                      mass,
                      region_length,
                      step)

    # Calculate the norm of the COR
    cors = [norm(pointx[0][0], pointx[0][1]) for pointx in circle.mapping]
    # Calculate norm of f
    forces = [norm(pointx[1][0], pointx[1][1]) for pointx in circle.mapping]
    # Select torques
    torques = [pointx[1][2] for pointx in circle.mapping]

    horizontal_axis = [0.1*xs for xs in range(int(sqrt(2)*region_length/step))]
    fig, (ax1, ax2) = plt.subplots(1, 2)

    f_approx = [circle.fmax*tanh(1.3*cor/radius) for cor in horizontal_axis]
    m_approx = [torque_approx(cor, circle.mmax, radius)
                for cor in horizontal_axis]

    ax1.scatter(cors, forces, label="limit surface")
    ax1.plot(horizontal_axis, f_approx, color="orange", label="approximation")
    ax2.scatter(cors, torques)
    ax2.plot(horizontal_axis, m_approx, color="orange")

    plt.show()
