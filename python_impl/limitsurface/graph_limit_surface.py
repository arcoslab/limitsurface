#!/usr/lib/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2017-2024 Autonomous Robots and Cognitive Systems Laboratory
# Author: Daniel Garcia-Vaglio <degv364@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt


from limitsurface.circle_ls import CircleLS
from limitsurface.rectangle_ls import RectangleLS


def main():
    fig = plt.figure()
    friction_coef = 0.5
    mass = 3
    cylinder_radius = 0.05
    rectangle_width = cylinder_radius * 2
    rectangle_height = cylinder_radius * 2
    region_length = cylinder_radius * 2.4
    step = 0.02

    circle = CircleLS(cylinder_radius,
                      friction_coef,
                      mass,
                      region_length,
                      step)

    # Separate fx, fy and m into different lists
    xc, yc, zc = zip(*circle.points)

    # Obtain the ellipsoid
    ellip_circle = [point[2] for point in circle.tangent_approx]

    ax1 = fig.add_subplot(221, projection=Axes3D.name)
    ax2 = fig.add_subplot(222, projection=Axes3D.name)
    ax1.scatter(xc, yc, zc)
    ax2.scatter(xc, yc, ellip_circle)

    rectangle = RectangleLS(rectangle_width,
                            rectangle_height,
                            friction_coef,
                            mass,
                            region_length,
                            step)

    xr, yr, zr = zip(*rectangle.points)
    ellip_rect = [point[2] for point in rectangle.tangent_approx]
    ax3 = fig.add_subplot(223, projection=Axes3D.name)
    ax4 = fig.add_subplot(224, projection=Axes3D.name)
    ax3.scatter(xr, yr, zr)
    ax4.scatter(xr, yr, ellip_rect)

    print("Circle approximation error: ", str(circle.approximation_error()))
    print("Rect approximation error: ", str(rectangle.approximation_error()))

    plt.show()
