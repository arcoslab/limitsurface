# -*- coding: utf-8 -*-
#
# Copyright (c) 2017-2024 Autonomous Robots and Cognitive Systems Laboratory
# Author: Daniel Garcia-Vaglio <degv364@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# System imports
from numpy.linalg import norm
from numpy import array
from math import sqrt, pi, exp, atan, tanh, atan2, cos, sin
from scipy.integrate import dblquad
from multiprocessing import Pool
from copy import copy

# Project imports
from limitsurface.constants import GRAVITY as G
from limitsurface.multidimensional_iterator import (
    MultidimensionalIterator as Mi)


def _to_polar(x, y):
    theta = atan2(y, x)
    r = sqrt(x**2 + y**2)
    return (r, theta)


def _from_cylindrical(r, z, theta):
    x = r*cos(theta)
    y = r*sin(theta)
    return (x, y, z)


def _null_function(x):
    return 0


def _fy(u, m, a, cx, cy, x, y):
    """
    Force in y direction

    In the special case where the denominator approaches to 0,
    an equivalent expression must be evaluated. This is by solving
    by hand the limit

    :param u: Dynamic friction coefficient
    :param m: object's mass
    :param a: object's base area
    :param cx: x coordinate center of rotation
    :param cy: y coordinate center of rotation
    :param x: x coordinate point where force is calculated
    :param y: y coordinate point where force is calculated
    :return: the force at point x,y
    """
    den = sqrt((x - cx)**2 + (y - cy)**2)
    if den < 0.001:
        return u * (m * G / a)
    else:
        return u * (m * G / a) * (x - cx) / den


def _fx(u, m, a, cx, cy, x, y):
    """
    Force in x direction

    In the special case where the denominator approaches to 0,
    an equivalent expression must be evaluated. This is by solving
    by hand the limit

    :param u: Dynamic friction coefficient
    :param m: object's mass
    :param a: object's base area
    :param cx: x coordinate center of rotation
    :param cy: y coordinate center of rotation
    :param x: x coordinate point where force is calculated
    :param y: y coordinate point where force is calculated
    :return: the force at point x,y
    """
    den = sqrt((x - cx)**2 + (y - cy)**2)
    if den < 0.001:
        return u * (m * G / a)
    else:
        return u * (m * G / a) * (cy - y) / den


def _compute_point(bounds, inner_bounds, a, u, m, cx, cy, eps):
    """
    Apply the double integral to the forces over the base

    :param bounds: outer bounds for the integral
    :param inner_args: inner bounds for hollow shapes
    :param u: Dynamic friction coefficient
    :param m: object's mass
    :param cx: x coordinate center of rotation
    :param cy: y coordinate center of rotation
    :param eps: (optional) tolerance
    :return: 2 point map (c, f). Where c is the center of rotation
             and f is the force vector it produces (fx, fy, torque)
    """
    functions = (
        # Force axis x
        lambda y, x: _fx(u, m, a, cx, cy, x, y),
        # Force axis y
        lambda y, x: _fy(u, m, a, cx, cy, x, y),
        # Torque axis z
        lambda y, x: (x * _fy(u, m, a, cx, cy, x, y)
                      - y * _fx(u, m, a, cx, cy, x, y))
    )

    final_result = [0, 0, 0]
    for idx in range(3):

        result = dblquad(
            functions[idx],
            bounds[0],
            bounds[1],
            bounds[2],
            bounds[3],
            epsabs=eps,
            epsrel=eps)[0]

        inner_result = dblquad(
            functions[idx],
            inner_bounds[0],
            inner_bounds[1],
            inner_bounds[2],
            inner_bounds[3],
            epsabs=eps,
            epsrel=eps)[0]

        final_result[idx] = result - inner_result

    return ([cx, cy], final_result)


def _unwrapper(input_params):
    center_of_rotation, ls_object = input_params
    cx, cy = center_of_rotation

    bounds = type(ls_object)._bound_functions(*ls_object._bound_params)
    inner_bounds = type(ls_object)._inner_bound_functions(
        *ls_object._inner_bound_params)

    return _compute_point(bounds,
                          inner_bounds,
                          ls_object.area,
                          ls_object.friction_coefficient,
                          ls_object.mass,
                          cx,
                          cy,
                          eps=ls_object.eps)


class LimitSurface():
    def __init__(self,
                 friction_coefficient,
                 mass,
                 region_length,
                 step,
                 eps=0.00001):
        """
        Create the limit surface for an specific base shape

        :param friction_coefficient: Dynamic friction coefficient
        :param mass: object's mass
        :param region_length: limits where COR (Center Of Rotation) are taken
        :param step: space between consecutive CORs
        :param eps: (optional) tolerance
        """
        self.friction_coefficient = friction_coefficient
        self.mass = mass
        self.region_length = region_length
        self.step = step
        self.eps = eps

        self._fmax = None
        self._mmax = None
        self._map = None
        self._points = None
        self._ellipsoid = None
        self._tangent_approx = None

        self._inner_bound_params = [0]

    @property
    def mapping(self):
        """
        Compute the map of centers of rotation to Limit surface points

        :return: a list of pairs where the first point is the center of
                 rotation and the second is the point in the limit surface
        """
        if self._map is None:
            it_bounds = [(-self.region_length, self.region_length,
                          self.step)] * 2
            const = self
            it = Mi(it_bounds, const, lambda x: x)
            pool = Pool()
            self._map = pool.map(_unwrapper, it)

        return self._map

    @property
    def points(self):
        """
        Get only the points of the limit surface without the centers of
        rotation
        """
        if self._points is None:
            self._points = [pair[1] for pair in self.mapping]
        return self._points

    def ls_point(self, center_of_rotation):
        """
        Calculate the point of the limit surface that results from the given
        center of rotation

        :param center_of_rotation: x,y coordinated of the COR
        :return: x_force, y_force, z_torque for that COR (rotating clockwise)
        """

        bounds = type(self)._bound_functions(*self._bound_params)
        inner_bounds = type(self)._inner_bound_functions(
            *self._inner_bound_params)

        return _compute_point(
            bounds,
            inner_bounds,
            self.area,
            self.friction_coefficient,
            self.mass,
            center_of_rotation[0],
            center_of_rotation[1],
            self.eps)[1]

    @property
    def ellipsoid(self):
        """
        Compute the points that make the ellipsoid approximation
        """
        def ellipsoid_point(x, y):
            return self.mmax * sqrt(1 - (x**2 + y**2)/self.fmax**2)

        if self._ellipsoid is None:
            self._ellipsoid = [
                (point[0], point[1], ellipsoid_point(point[0], point[1]))
                for point in self.points
            ]

        return self._ellipsoid

    @property
    def tangent_approx(self):
        """
        Compute the points that make the tangent approximation
        """
        a = sqrt(self.area/pi)

        def torque_approx(r):
            if abs(r) < 1:
                return self.mmax*(1-2/pi*atan(1.3*(r/a)**2))
            else:
                return self.mmax/(2.5*abs(r/a))

        def force_approx(r):
            return self.fmax*tanh(1.3*r/a)

        def theta_approx(theta):
            return theta - pi/2

        if self._tangent_approx is None:
            # Extract the Centers of rotation
            cors = [pointx[0] for pointx in self.mapping]
            # Transform to polar coordinates
            polars = [_to_polar(*cor) for cor in cors]

            # Calculate the tangent approximation
            self._tangent_approx = [_from_cylindrical(
                force_approx(polar[0]),
                torque_approx(polar[0]),
                theta_approx(polar[1])
            ) for polar in polars]

        return self._tangent_approx

    def ellipsoid_error(self, size_compensation=False):
        """
        The approximation error is the amount of error between the actual limit
        surface and the ellipsoid that is used to approximate it.
        """
        result = 0
        for idx in range(len(self.points)):
            result += abs((self.points[idx][2] - self.ellipsoid[idx][2]) /
                          norm(self.points[idx]))

        if size_compensation:
            compensation = 0.2737*(1-exp(-sqrt(self.area)/(5*sqrt(pi))))
        else:
            compensation = 1

        return result / (len(self.points) * compensation)

    def tangent_error(self):
        result = 0
        for idx in range(len(self.points)):
            result += (
                norm(array(self.points[idx])-array(self.tangent_approx[idx])) /
                norm(array(self.points[idx]))
                )
        return result / len(self.points)

    def approximation_error(self):
        return (self.ellipsoid_error(), self.tangent_error())

    @property
    def fmax(self):
        """
        Return the maximum translation force for the object

        This is a property because it depends on the base parameters and
        should not be overwritten
        """
        if self._fmax is None:
            self._fmax = self.mass * G * self.friction_coefficient

        return copy(self._fmax)

    @property
    def mmax(self):
        """
        Calculate the maximum rotation torque for the object
        When there is no analytic solution derived yet, use the integral.
        In this case, would be to calculate when the Center of Rotation is
        at the center of the object
        """
        if self._mmax is None:
            self._mmax = self.ls_point((0, 0))[2]
        return copy(self._mmax)

    @classmethod
    def _inner_bound_functions(cls, *args):
        """
        Most base shapes are not hollow so they do not need an inner
        bound function
        """
        return [0, 0, lambda x: 0, lambda x: 0]
