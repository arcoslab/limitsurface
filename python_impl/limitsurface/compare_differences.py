#!/usr/lib/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2017-2024 Autonomous Robots and Cognitive Systems Laboratory
# Author: Daniel Garcia-Vaglio <degv364@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from numpy.linalg import norm

from limitsurface.circle_ls import CircleLS


def calculate_difference(first, second):
    # For this to work both limit surfaces should have the exact same Centers
    # of rotation.
    result = 0
    for first_inst in first.mapping:
        cor_1 = first_inst[0]
        for second_inst in second.mapping:
            cor_2 = second_inst[0]
            if cor_1 == cor_2:
                f1 = np.array(first_inst[1])
                f2 = np.array(second_inst[1])
                result += norm(f1 - f2) / norm(f1)
    return result / len(first.mapping)


def main():
    radius = 1
    factor = 1.1
    mu = 0.5
    mass = 1

    first = CircleLS(radius, mu, mass, 2, 0.2, eps=0.001)

    # Variance of mu and mass
    print("starting variance of mu and mass")
    mus = [mu*pow(factor, idx-2) for idx in range(5)]
    masses = [mass*pow(factor, idx-2) for idx in range(5)]
    results_mum = []
    for mux in mus:
        for massx in masses:
            second = CircleLS(radius, mux, massx, 2, 0.2, eps=0.001)
            results_mum.append(
                (mux, massx, calculate_difference(first, second)))

    # Wide variance of mu times mass
    print("Starting variance of mu times mass")
    masses = [0.1 * (idx+1) for idx in range(20)]
    results_mass = []
    for massx in masses:
        second = CircleLS(radius, mu, massx, 2, 0.2, eps=0.001)
        results_mass.append((massx, calculate_difference(first, second)))

    # Changes in radius
    print("Starting variance or r")
    radi = [0.1 * (idx+1) for idx in range(20)]
    results_radi = []
    for rx in radi:
        second = CircleLS(rx, mu, mass, 2, 0.2, eps=0.001)
        results_radi.append((rx, calculate_difference(first, second)))

    print("Variance friction and mass")
    for result in results_mum:
        mu, mass, diff = result
        print(round(mu, 4), round(mass, 4), round(diff, 4))

    print("Variance friction times mass")
    for result in results_mass:
        mass, diff = result
        print(round(mass, 4), round(diff, 4))

    print("Variance radius")
    for result in results_radi:
        rx, diff = result
        print(round(rx, 4), round(diff, 4))
