#+title: The Limit Surface
#+author: Daniel García Vaglio

* Calculating the limit surface

The limit surface is a tool that enables us have a relation between the forces
an object experiences due to dynamic friction and the velocity it has. Couloumb
equation gives us the friction in one point, so integrating over the entire
surface of an object should give the result for a particular face. First we
define a center of rotation (COR) defined by $(c_x, c_y)$. If the object moves
around that center of rotation in a clockwise direction then the force of
friction at any point will be given by $(\phi_x, \phi_y)$ where $\phi_x$ and
$\phi_y$ are defined by the following equations. Where $\mu$ is the friction
coefficient, $m$ is the mass of the object, and $a$ is the area.

\[
\phi_x = \mu \frac{mg}{a}\frac{c_y-y}{\sqrt{(x-c_x)^2 + (y-c_y)^2}}
\]

\[
\phi_y = \mu \frac{mg}{a}\frac{x-c_x}{\sqrt{(x-c_x)^2 + (y-c_y)^2}}
\]

Then the torque $\rho$ is defined by:

\[
\rho = x \phi_y - y \phi_x
\]

Then to obtain the total forces that are producing for moving around the COR, it
is necessary to integrate these functions over the entire surface of the object.

\[
f_x = \int_{x_{low}}^{x_{high}}\int_{y(x)_{low}}^{y(x)_{high}} \phi_x dy dx
\]
\[
f_y = \int_{x_{low}}^{x_{high}}\int_{y(x)_{low}}^{y(x)_{high}} \phi_y dy dx
\]
\[
\tau = \int_{x_{low}}^{x_{high}}\int_{y(x)_{low}}^{y(x)_{high}} \rho dy dx
\]

A point in the limit surface is defined as $(f_x, f_y, \tau)$. It is important
to note the extremes of this surface, $f_{max}$ is defined as the maximum
translation force an object can experience from friction, and $\tau_{max}$ the
maximum torque. $f_{max}$ is obtained when there is only translation without
rotation,then $f_{max} = mg\mu$. $\tau_{max}$ is obtained when there is no
translation and only rotation, so to solve it one must solve the integral when
$(c_x, c_y) = (0, 0)$.

* Ellipsoid approximation

The real limit surface is not usually used during the control process, instead
we use an approximation. An ellipsoid has been used. For each point of the limit
surface $f = (f_x, f_y, \tau)$ one can calculate the corresponding point of the
approximation ellipsoid $p = (p_x, p_y, p_z)$. It should be noted that this
approximation point is not necessarily the closest, but the one that shares the
same x, y coordinates. So the approximation point is defined as:

\[p_x = f_x\]

\[p_y = f_y\]
\[p_z = \tau_{max} \sqrt{1-\frac{f_x^2+f_y^2}{f_{max}^2}}\]

* Define the approximation error

To have an idea about the error of this approximation we can calculate various
points of the limit surface and compare them to their ellipsoid counterparts.
Let $N$ be the total number of points that have been calculated, then $f_k$ is
the k-th point of the limit surface (according to an arbitrary ordering), and
$p_k$, is the corresponding approximation to $f_k$. The error $\epsilon$ is as
follows:

\[ \epsilon = \frac{1}{N}\sum_{k=1}^N \left|\frac{f_k-p_k}{f_k}\right| \]

To test this error function, several limit surfaces were calculated for circle
bases as well as square bases. In all cases the square sides where calculated
such that both the circle and the square had the same area, so only the radius
($r$) values are given.


Results for varying $\mu$, $m=1$, $r=1$, $N=100$

| $\mu$ | $\epsilon$ circle | $\epsilon$ square |
|-------+-------------------+-------------------|
|   0.1 |            0.2741 |            0.2871 |
|   0.2 |            0.2741 |            0.2871 |
|   0.3 |            0.2741 |            0.2871 |
|   0.4 |            0.2741 |            0.2871 |
|   0.5 |            0.2741 |            0.2871 |

Results for varying $m$, $\mu=0.5$, $r=1$, $N=100$

|   $m$ | $\epsilon$ circle | $\epsilon$ square |
|-------+-------------------+-------------------|
| 0.125 |            0.2741 |            0.2871 |
|  0.25 |            0.2741 |            0.2871 |
|   0.5 |            0.2741 |            0.2871 |
|     1 |            0.2741 |            0.2871 |
|     2 |            0.2741 |            0.2871 |
|     4 |            0.2741 |            0.2871 |
|     8 |            0.2741 |            0.2871 |
|    16 |            0.2741 |            0.2871 |
|    32 |            0.2741 |            0.2871 |

Results for varying $r$, $m=1$, $\mu=0.5$, $N=100$

|   $r$ | $\epsilon$ circle | $\epsilon$ square |
|-------+-------------------+-------------------|
| 0.125 |            0.2741 |            0.2871 |
|  0.25 |            0.2741 |            0.2871 |
|   0.5 |            0.2741 |            0.2871 |
|     1 |            0.2741 |            0.2871 |
|     2 |            0.2741 |            0.2871 |
|     4 |            0.2741 |            0.2871 |
|     8 |            0.2741 |            0.2871 |
|    16 |            0.2741 |            0.2871 |
|    32 |            0.2741 |            0.2871 |

Results for varying $N$, $r=1$, $m=1$, $\mu=0.5$

|   $N$ | $\epsilon$ circle | $\epsilon$ square |
|-------+-------------------+-------------------|
|    64 |            0.2752 |            0.2878 |
|   100 |            0.2741 |            0.2871 |
|   144 |            0.2695 |            0.2867 |
|   196 |            0.2728 |            0.2863 |
|   256 |            0.2724 |             0.286 |
|   324 |            0.2694 |            0.2858 |
|   400 |            0.2694 |             0.284 |
|   900 |            0.2711 |            0.2851 |
|  1600 |            0.2694 |             0.284 |
|  2500 |            0.2694 |            0.2847 |
|  3600 |            0.2694 |             0.284 |
|  4900 |            0.2701 |             0.284 |
|  6400 |              0.27 |            0.2845 |
|  8100 |              0.27 |            0.2844 |
| 10000 |            0.2694 |            0.2844 |


From these results it is safe to conclude that the error functions does not
depend on the parameters of the object. This error function really gives a value
for the intrinsic error of the approximation given the base shape.

* Errors of other base shapes

There is enough evidence to suggest that the error function is an intrinsic
property of the base of the object, so other base shapes should be explored.

An elongated rectangle deforms the limit surface. So for $\mu=0.5$, $m=1$, and
$N=100$, one side is $l_1=1$, and vary $l_2$. The table shows that the lowest
error is achieved when the rectangle is a square.


| $l_2$ | $\epsilon$ |
|-------+------------|
|   0.1 |     0.8598 |
|   0.2 |     0.6066 |
|   0.3 |     0.4797 |
|   0.4 |     0.4027 |
|   0.5 |     0.3602 |
|   0.6 |     0.3352 |
|   0.7 |     0.3195 |
|   0.8 |       0.31 |
|   0.9 |     0.3044 |
|   1.0 |     0.2871 |

Not all cylinder containers have a circle base, in many cases the area of
contact with the table is a ring. So for $\mu=0.5$, $m=1$, $N=100$, $r=1$, the
internal radius $r_i$ was varied. The "more hollow" the ring is, the higher the
error. The error is still similar to the full circle, and seems to reach the
maximum point around $r_i = 0.7 r$

| $r_i$ | $\epsilon$ |
|-------+------------|
|   0.0 |     0.2741 |
|   0.1 |     0.2789 |
|   0.2 |     0.2894 |
|   0.3 |     0.3012 |
|   0.4 |     0.3115 |
|   0.5 |      0.319 |
|   0.6 |      0.323 |
|   0.7 |     0.3237 |
|   0.8 |     0.3212 |
|   0.9 |     0.3162 |
| 0.999 |     0.3093 |

* Difference between limit surfaces

Let $\vec{f}(\vec{c})=(f_x(\vec{c}), f_y(\vec{c}), \tau(\vec{c}))$ be the point
of the limit surface that results from the object moving around the center of
rotation $\vec{c} = (c_x, c_y)$. Consider two limit surfaces,
$\vec{f}_1(\vec{c})$ is a point of the first one and $\vec{f}_2(\vec{c})$ a
point of the second one that result of the movement around the same point
$\vec{c}$. Both limit surfaces are represented by $N$ points, where each point
is the result of a particular center of rotation $\vec{c}_k$. The difference of
the second limit surface with respect to the first one is defined as:

\[ \delta_1^2 = \frac{1}{N} \sum_{k=1}^N\frac{\lVert  \vec{f}_1(\vec{c}_k) - \vec{f}_2(\vec{c}_k)\rVert}{\lVert \vec{f}_1(\vec{c}_k)\rVert}\]

It should be noted that we are not comparing the two closest points of each
limit surface, but rather the points that result from the same movement
velocity. Although from a geometric perspective the first one makes more sense,
from a physics perspective the second one is more useful.

The first test is to determine how changes in $\mu$ and $m$ affect the limit
surface. The following table shows the difference between two limit surfaces
where the first one is constant with $\mu_1=0.5$, $m_1=1$, $r_1=1$ and
$N_1=100$. The second one changes $\mu_2$ and $m_2$ but $r_1=r_2$ and $N_1=N_2$.
$a=1.1$.


| $\delta_1^2$        | $m_2=m_1a^{-2}$ | $m_2=m_1a^{-1}$ | $m_2=m_1$ | $m_2=m_1a$ | $m_2=m_1a^2$ |
|---------------------+-----------------+-----------------+-----------+------------+--------------|
| $\mu_2=\mu_1a^{-2}$ |           0.317 |          0.2487 |    0.1736 |     0.0909 |          0.0 |
| $\mu_2=\mu_1a^{-1}$ |          0.2487 |          0.1736 |    0.0909 |        0.0 |          0.1 |
| $\mu_2=\mu_1$       |          0.1736 |          0.0909 |       0.0 |        0.1 |         0.21 |
| $\mu_2=\mu_1a$      |          0.0909 |             0.0 |       0.1 |       0.21 |        0.331 |
| $\mu_2=\mu_1a^2$    |             0.0 |             0.1 |      0.21 |      0.331 |       0.4641 |

As expected whenever $\mu_1m_1=\mu_2m2$ the resulting limit surfaces are the
same. Given that these two terms are always multiplicating each other in the
equations, that is exactly the behaviour one should expect. So differences in
$\mu$ are equivalent to differences in $m$ by the same factor.

Now we can study what happens when one varies $\mu m$, but the other parameters
are constant (namely $r=1$, $N=100$): In this case $\mu_1 m_1 = 0.5$ and $\mu_2
m_2$ changes. The behaviour is linear.

| $\frac{\mu_2 m_2}{\mu_1 m_1}$ | $\delta_1^2$ |
|-------------------------------+--------------|
|                           0.1 |          0.9 |
|                           0.2 |          0.8 |
|                           0.3 |          0.7 |
|                           0.4 |          0.6 |
|                           0.5 |          0.5 |
|                           0.6 |          0.4 |
|                           0.7 |          0.3 |
|                           0.8 |          0.2 |
|                           0.9 |          0.1 |
|                           1.0 |          0.0 |
|                           1.1 |          0.1 |
|                           1.2 |          0.2 |
|                           1.3 |          0.3 |
|                           1.4 |          0.4 |
|                           1.5 |          0.5 |
|                           1.6 |          0.6 |
|                           1.7 |          0.7 |
|                           1.8 |          0.8 |
|                           1.9 |          0.9 |
|                           2.0 |          1.0 |


Now we can see what happens when the radius is the one that changes. $\mu=0.5$,
$m=1$, $N=100$. $r_1=1$ and $r_2$ changes.

| $\frac{r_2}{r_1}$ | $\delta_1^2$ |
|-------------------+--------------|
|               0.1 |        0.275 |
|               0.2 |       0.2654 |
|               0.3 |       0.2489 |
|               0.4 |       0.2274 |
|               0.5 |       0.2006 |
|               0.6 |       0.1693 |
|               0.7 |       0.1333 |
|               0.8 |        0.093 |
|               0.9 |       0.0485 |
|               1.0 |          0.0 |
|               1.1 |       0.0524 |
|               1.2 |       0.1084 |
|               1.3 |        0.168 |
|               1.4 |        0.231 |
|               1.5 |       0.2974 |
|               1.6 |       0.3668 |
|               1.7 |       0.4392 |
|               1.8 |       0.5144 |
|               1.9 |       0.5922 |
|               2.0 |       0.6723 |


Finally we can compare circles and squares. $f_{max} = mg\mu$, which means that
for two objects, one cylinder and one box, as long as they share the same
friction coefficient and mass, their maximum force will be the same. What
changes is the torque. But it is important to note that $f_{max}$ does not depend
on the size of the base of the object, so by varying the base one should be able
to find a cylinder and a box that share the same $\tau_{max}$. Let the box be a
square of side $l$ and the cylinder of radius $r$. Both share the same mass $m$
and friction coefficient $\mu$.

The $\tau_{max}$ of the box is:
\[ \frac{\mu mg l}{6} (\sqrt{2}+arcsinh(1))\]

the $\tau_{max}$ of the cylinder is:
\[ \frac{2\mu mg r}{3}\]

Then the value of $r$ is:

\[ r = \frac{l}{4} (\sqrt{2}+arcsinh(1))\]

At this radius the limit surface of the cylinder and the limit surface of the
box would have the exact same ellipsoid approximation. And thus would be
indistinguishable from one another in our simulator.

TODO: more on this later.


* A new approximation

One way of looking at the limit surface is as a function that takes centers of
rotation and computes the forces that the object experiences. In that sense the
structure that we usually plot as the limit surface is just the set of images of
that function. 

In the case of the circle, it is known that there is axial symmetry, so it
doesn't matter the angle of $\vec{c}$. In other words $\lVert (f_x, f_y,
\tau)\rVert$ depends only on $\lVert\vec{c}\rVert$. It is therefore useful to
use polar and cylindrical coordinates instead of Cartesian coordinates for this
analysis. Then let $\vec{c} = (r_c, \theta_c)$ and $\vec{f}=(r_f, \tau,
\theta_f)$. Note that in both Cartesian and cylindrical the value of $\tau$ is
the same. The following plot shows $r_f$ with respect to $r_c$ , and $\tau$ with
respect to $r_c$.

[[./media/f_as_function_of_r.png]]

To get to a new approximation one can start by studying only one part of the
problem. It is safe to say that the limit surface of the circle has radial
symmetry. So we can study the limit surface from points only in the $x$ axis. In
this case $\vec{c} = (c_x, 0)$, so for simplicity $\vec{c} = c$ then $\phi_x =
0$. $\phi_y$ and $\rho$ can be simplified to:

\[ \phi_y(c) =\frac{ \mu mg}{a}\frac{x-c}{\sqrt{(x-c)^2+y^2}} \]
\[\rho(c) = x \phi_y(c)\]


Then:

\[f(c) = \int_{-r}^{r}\int_{-\sqrt{r^2-x^2}}^{\sqrt{r^2-x^2}} \frac{\mu mg
(x-c)}{a \sqrt{(x-c)^2 +y^2}} dy dx\]
