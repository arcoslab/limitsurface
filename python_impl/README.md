# Limit Surface

This repository is a collection of tools that help the user manipulate Limit
surfaces.

## Project

This is a poetry project, poetry will manage versions and dependencies
automatically. It will also isolate the project from your system install using a
virtual environment. The virtual environment is hidden from the user.

To install poetry:

```
pipx install poetry
```

Once you have poetry, you can install all the dependencies in the virtual
environment (run it at the root of the project):

```
poetry install
```

To install the pre-commit hooks use:
```
poetry run pre-commit install
```

To run the plotter app execute the following command. This will give you a plot
side by side of the limit surface and the approximation ellipsoid.

```
poetry run plot
```
## Style

This project is compliant with PEP8. It is therefore very important to never
commit code that is not compliant with that standard.

```
poetry run flake8
```

If you installed the pre-commit hooks, then this verification will run
automatically before every commit.
